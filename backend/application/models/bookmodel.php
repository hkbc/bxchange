<?php
class Bookmodel extends CI_Model {

	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}


	public function get_users_books($user_id){
		$sql = 'SELECT b.*,
					t.id as transactions_id,
					t.state
				FROM transactions t
				LEFT JOIN books b ON t.book_id = b.id 
				LEFT JOIN users u ON u.id = t.owner_id
				WHERE t.owner_id = ?';
		return $this->db->query($sql, array($user_id))->result_array();
	}

	public function get_available_books($fs = NULL){
		$where = '';
		if($fs !== NULL){
			$fs = '%' . $fs . '%';
			$where .= ' AND (b.title LIKE ? OR b.description LIKE ? OR b.authors LIKE ?)';
		}

		$sql = 'SELECT b.*, 
					t.id as transactions_id,
					u.id as user_id,
					u.username as owner_username,
					u.first_name as owner_first, 
					u.last_name as owner_last
				FROM transactions t
				LEFT JOIN books b ON t.book_id = b.id 
				LEFT JOIN users u ON u.id = t.owner_id
				WHERE t.state = "active"';
		if($fs !== NULL){
			$sql .= $where;
			$sql .= ' ORDER BY t.id DESC';
			$query = $this->db->query($sql, array($fs, $fs, $fs));
		} else {
			$sql .= ' ORDER BY t.id DESC';
			$query = $this->db->query($sql);
		}
		return $query->result_array();
	}

	public function get_latest_book_id(){
		$sql = 'SELECT t.id as transaction_id
				FROM transactions t
				LEFT JOIN books b ON t.book_id = b.id 
				LEFT JOIN users u ON u.id = t.owner_id
				ORDER by t.id DESC LIMIT 1';
		$result = $this->db->query($sql)->result_array();
		return $result[0];
		
	}
	public function addbooks_by_barcode($data){
		//check if ISBN exist
		$query = $this->db->get_where('books', array('ISBN_13' => $data['ISBN_13']), 1, 0);
		$row = $query->result_array();
		if(isset($row[0])){
			return $row[0]['id'];

		//if ISBN not exist, add a new row
		} else {
			$query = $this->db->insert('books',$data);	
			if($insert_id = $this->db->insert_id()){
				return $insert_id;
			} else {
				return false;
			}
		}
	}

	/**
	 * add_transactions
	 * @param int $book_id
	 * @param int $user_id
	 * @return int
	 */
	public function add_transactions($book_id, $user_id)
	{
		//Build the data
		$data['owner_id'] = $user_id;
		$data['book_id'] = $book_id;
		$data['state'] = 'active';

		//insert the row
		$query = $this->db->insert('transactions', $data);
		if($insert_id = $this->db->insert_id()){
			return $insert_id;
		} else {
			return false;
		}
	}


	public function books_requested_by_uid($user_id){
		$sql = 'SELECT b.*, t.id AS transaction_id, t.state 
				FROM transactions t
				LEFT JOIN books b ON t.book_id = b.id 
				LEFT JOIN users u ON u.id = t.owner_id
				WHERE t.adopter_id = ?
				';
		return $this->db->query($sql, array($user_id))->result_array();
	}

/**
 *
 * Making request and accept request
 *
 */
	//available method: credit, donation, exchange
	public function request_book($transaction_id, $user_id, $method, $book_id = NULL)
	{	
		//check if the request existed
		$check_query = $this->db->get_where('requests', array('transaction_id' => $transaction_id, 'requester_id' => $user_id));
		$check_row = $check_query->result_array();
		if(isset($check_row[0]))
		{
			return $check_row[0]['id'];
		}

		//build data
		$data['transaction_id'] = $transaction_id;
		$data['requester_id'] = $user_id;
		$data['method'] = $method;
		if($data['method'] == 'exchange'){
			$data['exchange_book_id'] = $book_id;
		}else{
			$data['exchange_book_id'] = NULL;
		}

		//insert into request table
		$this->db->insert('requests', $data);
		return $this->db->insert_id();

	}

	public function get_request_list($transaction_id)
	{
		$sql = 'SELECT u.id AS user_id, u.username, u.first_name, u.last_name, r.method, r.exchange_book_id, r.state, b.*
				FROM requests r
				LEFT JOIN transactions t ON r.transaction_id = t.id
				LEFT JOIN users u ON u.id = t.owner_id
				LEFT JOIN books b ON b.id = r.exchange_book_id
				WHERE r.state != "cancelled" AND t.id = ?';

		return $this->db->query($sql, $transaction_id)->result_array();
	}

	public function get_book_transactions($book_id){
		$sql = 'SELECT t.id as transaction_id, u.id as doner_uid, u.username, u.first_name, u.last_name, t.state
				FROM transactions t
				LEFT JOIN users u ON u.id = t.owner_id
				LEFT JOIN requests r ON r.transaction_id = t.id
				WHERE t.book_id = ? 
				ORDER BY t.id DESC';
		return $this->db->query($sql, $book_id)->result_array();
	}
}
