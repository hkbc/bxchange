<?php
class Usermodel extends CI_Model {

	public function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_user_detail_by_uid($user_id)
	{
		$sql = 'SELECT id, username, first_name, last_name, credit
				FROM users
				WHERE id = ?';
		return $this->db->query($sql, array($user_id))->result_array();
	}	

	public function get_user_detail_by_username($username)
	{
		$sql = 'SELECT id, username, first_name, last_name, credit
				FROM users
				WHERE username = ?';
		return $this->db->query($sql, array($username))->result_array();
	}	


}
