<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Battlehackapi extends CI_Controller {

	public function index()
	{
		$this->load->view('battlehack');
	}


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bookmodel');
		$this->load->model('Usermodel');
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
	}

    public function send_sms(){
        require_once("../twilio-php/Services/Twilio.php");

        $sid = "AC323ea7788b1167e3b620e141d2a83daf"; // Your Account SID from www.twilio.com/user/account
        $token = "892da85ac6b9b49f5fb0a0142d9c3bda"; // Your Auth Token from www.twilio.com/user/account

        $from = "441473379413";
        $to = "447721710487";
        $msg = "Hi Chi, just would like to let you know, your book  'THE PAYPAL WARS' has requested by Daeus.";
        $client = new Services_Twilio($sid, $token);
         $message = $client->account->messages->sendMessage(
             $from, // From a valid Twilio number
             $to, // Text this number
             $msg
         );
        echo $message->sid;
    }

    public function login(){

		$this->load->database();
        $email =  $this->input->get_post("email");
        $first_name =  $this->input->get_post("first_name");
        $last_name =  $this->input->get_post("last_name");

        $result="";
        if(strlen($email)>5){


        $this->db->where('username', $email);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            $i = 0;
            $row = $query->row();
            $result = $row;
        }else {


            $this->db->set('username', $email);
            $this->db->set('first_name', $first_name);
            $this->db->set('last_name', $last_name);
            $this->db->set('email', $email);
            $this->db->insert('users');
            $id = $this->db->insert_id();

            $this->db->where('username', $email);
            $query = $this->db->get('users');
            if ($query->num_rows() > 0) {
                $i = 0;
                $row = $query->row();
                $result = $row;
            }
        }

        }
        echo json_encode($result);

    }
	public function latest_book()
	{
		$result = $this->Bookmodel->get_latest_book_id();
		echo json_encode($result);
	}

	public function getbook()
	{
		print_r($this->Bookmodel->getbooks());
	}

	public function getMD5($q)
	{
		echo md5($q);
	}
/*
	public function login($username, $pw)
	{
	
	}
*/
	public function get_user_details($user_id){
		$result = $this->Usermodel->get_user_detail_by_uid($user_id);
		echo json_encode($result);
	}

	public function get_user_details_by_username($username){
		$result = $this->Usermodel->get_user_detail_by_username($username);
		echo json_encode($result);
	}


	public function get_users_books($user_id){
		$result = $this->Bookmodel->get_users_books($user_id);
		echo json_encode($result);
	}

	public function get_available_book($fs = NULL){
		$result = $this->Bookmodel->get_available_books($fs);
		echo json_encode($result);
	}

	public function get_book_by_barcode($barcode){
		$bookInfo = $this->get_book_info($barcode);
		echo json_encode($bookInfo);
	}

	public function add_book_by_barcode($barcode, $user_id = NULL){
		$bookInfo = $this->get_book_info($barcode);
		$book_id = $this->Bookmodel->addbooks_by_barcode($bookInfo);
		if($book_id){
			//add transactions if book_id is found
			if($user_id !== NULL)
			{
				$this->Bookmodel->add_transactions($book_id, $user_id);
			}
			$json['success'] = 1;
		} else {
			$json['success'] = 0;
		}
		echo json_encode($json);
		return;
    }

	private function get_book_info($barcode)
	{
		$result =  file_get_contents("https://www.googleapis.com/books/v1/volumes?q=$barcode");

        $result = json_decode($result);

        //$bookInfo = new BookInfo();
		$bookInfo = array();

        if($result->totalItems > 0){
            $bookInfo['title'] = $result->items[0]->volumeInfo->title;
            $bookInfo['authors'] = implode (", ", $result->items[0]->volumeInfo->authors);
            $bookInfo['publishedDate'] = $result->items[0]->volumeInfo->publishedDate;
			$bookInfo['ISBN_13'] = '';
			if(isset($result->items[0]->volumeInfo->industryIdentifiers)){
				foreach($result->items[0]->volumeInfo->industryIdentifiers as $isbn_item)
				{
					if($isbn_item->type === 'ISBN_13')
					{
						$bookInfo['ISBN_13'] = $isbn_item->identifier;
					}
				}
			
			}

			if(isset($result->items[0]->volumeInfo->description)){
				$bookInfo['description'] = $result->items[0]->volumeInfo->description;
			} else {
				$bookInfo['description'] = '';
			}

            $bookInfo['thumbnail']  = $result->items[0]->volumeInfo->imageLinks->thumbnail;
            $bookInfo['links'] = $result->items[0]->volumeInfo->canonicalVolumeLink;
        }

		return $bookInfo;
	}

	public function make_request($transaction_id, $requester_id, $method, $book_id = NULL){
		$request_id = $this->Bookmodel->request_book($transaction_id, $requester_id, $method, $book_id);
		if($request_id){
			$json['success'] = 1;
			$json['request_id'] = $request_id;
		} else {
			$json['success'] = 0;
			$json['request_id'] = NULL;
		}
		echo json_encode($json);
	}

	public function get_request_list($transaction_id){
		$result = $this->Bookmodel->get_request_list($transaction_id);
		echo json_encode($result);
	}
	
	public function books_requested_by_uid($user_id){
		$result = $this->Bookmodel->books_requested_by_uid($user_id);
		echo json_encode($result);
	}

	public function get_book_transactions($book_id){
		$result = $this->Bookmodel->get_book_transactions($book_id);
		echo json_encode($result);
	}
}
