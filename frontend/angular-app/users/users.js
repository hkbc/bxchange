'use strict';

angular.module('users', ['services.API', 'services.OAuth'])
  .constant('USERS_ROUTES', {
    UsersShowController: {
      templateUrl: 'users/templates/show.html',
      controller: 'UsersShowController',
      resolve: {
        user: function ($route, UsersService) {
          return UsersService.find($route.current.params.username);
        },
      },
    },
  });
