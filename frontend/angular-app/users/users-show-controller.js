'use strict';

angular.module('users')
  .controller('UsersShowController', function ($scope, user) {
    $scope.user = user;
  });
