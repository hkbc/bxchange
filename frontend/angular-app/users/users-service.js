'use strict';

angular.module('users')
  .service('UsersService', function ($q, $rootScope, ApiService, GplusService) {
    var exports = this


    $rootScope.currentUser = null;
    $rootScope.$watch('currentUser', function (user) {
      if (user) {
        sessionStorage.user = JSON.stringify(user);
      } else {
        sessionStorage.removeItem('user');
      }
    });


    exports.authenticateWith = function (method) {
      var deferred = $q.defer();

      switch (method) {
        case 'gplus':
          GplusService.authenticate().then(function (data) {
            $rootScope.currentUser = data;
            deferred.resolve(data);
          });
          break;

        default:
          deferred.reject('Unsupported authentication method ' + method);
      }

      return deferred.promise;
    };


    exports.find = function (username) {
      var deferred = $q.defer();

      ApiService.request({ method: 'GET', url: '/get_user_details_by_username/' + username })
        .then(function (response) {
          deferred.resolve(response.data[0]);
        }, function (response) {
          console.error(response);
          deferred.reject(response);
        });

      return deferred.promise;
    };


    return exports;
  }).run(function ($rootScope) {
    $rootScope.currentUser = (sessionStorage.user) ? JSON.parse(sessionStorage.user) : null
  });
