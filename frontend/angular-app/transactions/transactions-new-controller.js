'use strict';

angular.module('transactions')
  .controller('TransactionsNewController', function ($scope, $location, $http,
      FlashService, ApiService, book, exchangeables) {
    var _initialized = false,
        _descriptions = {
          credit: 'Offer to trade the book for xchange credits',
          exchange: 'Offer to exchange the book for another title',
          donation: 'Offer to make a donation to charity in exchange for the book',
          coffee: 'Buy this person a coffee',
        };
    $scope.book = book;
    $scope.exchangeables = exchangeables;
    $scope.methodDescription = null;

    $scope.onSubmitForm = function (form) {
      FlashService.pushMessage('Your bid for "' + book.title + '" has been successfully submitted');
      $location.path('/book/' + book.id);
    };

    $scope.$watch('form.method', function (newMethod) {
      $scope.methodDescription = _descriptions[newMethod];

      switch (newMethod) {
        case 'coffee':
          if (!_initialized) {
            _initialized = true;
            braintree.setup('eyJ2ZXJzaW9uIjoxLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJlYTQwY2Q1ZTliM2NmZTE3OTNjM2FmZGViMDQ2ZjgwNWNlNTE1MmJjMDA2Y2VhZWRhMjlhZDY1MzMyOTM4Y2YzfGNyZWF0ZWRfYXQ9MjAxNC0xMC0xMVQyMzo0MTowOC44NDA1MzAyODcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWRjcHNweTJicndkanIzcW5cdTAwMjZwdWJsaWNfa2V5PTl3d3J6cWszdnIzdDRuYzgiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwicGF5bWVudEFwcHMiOltdLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9jbGllbnQtYW5hbHl0aWNzLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20ifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6ZmFsc2UsInBheXBhbEVuYWJsZWQiOnRydWUsInBheXBhbCI6eyJkaXNwbGF5TmFtZSI6IkFjbWUgV2lkZ2V0cywgTHRkLiAoU2FuZGJveCkiLCJjbGllbnRJZCI6bnVsbCwicHJpdmFjeVVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS9wcCIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vdG9zIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6dHJ1ZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwibWVyY2hhbnRBY2NvdW50SWQiOiJzdGNoMm5mZGZ3c3p5dHc1IiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn19', 'dropin', {
              container: 'dropin',
            });
          }
      }
    });

    $scope.payForCoffee = function () {
      $http({ method: 'GET', url: 'https://hack.ngrok.com/daeus/battlehack/paypal/checkout.php' })
        .then(function () {
          FlashService.pushMessage('You have successfully paid for a coffee');
          $location.path('/');
        });
    };
  });
