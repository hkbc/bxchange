'use strict';

angular.module('transactions', ['ngRoute', 'services.API', 'flash', 'books'])
  .constant('TRANSACTIONS_ROUTES', {
    TransactionsController: {
      templateUrl: 'transactions/templates/index.html',
      controller: 'TransactionsController',
      resolve: {
        transactions: function ($route, TransactionsService) {
          return TransactionsService.where({ id: $route.current.params.id });
        },
      },
    },
    TransactionsForUsersController: {
      templateUrl: 'transactions/templates/index-for-user.html',
      controller: 'TransactionsForUsersController',
      resolve: {
        transactions: function ($rootScope, TransactionsService) {
          return TransactionsService.where({ userId: $rootScope.currentUser.id });
        },
      },
    },
    TransactionsNewController: {
      templateUrl: 'transactions/templates/new.html',
      controller: 'TransactionsNewController',
      resolve: {
        book: function ($route, BooksService) {
          return BooksService.find({ id: $route.current.params.id });
        },
        exchangeables: function ($rootScope, TransactionsService) {
          return TransactionsService.where({ userId: $rootScope.currentUser.id });
        },
      },
    },
  });
