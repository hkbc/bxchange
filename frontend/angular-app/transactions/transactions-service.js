'use strict';

angular.module('transactions')
  .service('TransactionsService', function ($q, ApiService) {
    var exports = this;

    exports.where = function (options) {
      var deferred = $q.defer(),
          requestOptions = { method: 'GET' };

      if (options.id) {
        requestOptions.url = '/get_request_list/' + options.id;
      } else if (options.bookId) {
        requestOptions.url = '/get_book_transactions/' + options.bookId;
      } else if (options.userId) {
        requestOptions.url = '/books_requested_by_uid/' + options.userId;
      }

      ApiService.request(requestOptions)
        .then(function (response) {
          deferred.resolve(response.data || []);
        }, function (response) {
          console.error(response);
          deferred.reject(response);
        });

      return deferred.promise;
    };

    return exports;
  });
