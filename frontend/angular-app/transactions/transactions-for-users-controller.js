'use strict';

angular.module('transactions')
  .controller('TransactionsController', function ($scope, transactions) {
    $scope.transactions = transactions;
  });
