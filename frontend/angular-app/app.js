'use strict';

angular.module('bxchange', ['ngRoute', 'templates', 'flash', 'visitors', 'books', 'users', 'transactions'])
  .config(function ($routeProvider, $locationProvider, VISITORS_ROUTES, BOOKS_ROUTES,
                    USERS_ROUTES, TRANSACTIONS_ROUTES) {
    $locationProvider.html5Mode(true);

    $routeProvider
      .when('/', BOOKS_ROUTES.BooksController)
      .when('/register', VISITORS_ROUTES.VisitorsController)
      .when('/books', BOOKS_ROUTES.BooksController)
      .when('/book/:id', BOOKS_ROUTES.BooksShowController)
      .when('/book/:id/transactions/new', TRANSACTIONS_ROUTES.TransactionsNewController)
      .when('/user/:username', USERS_ROUTES.UsersShowController)
      .when('/transactions/:id', TRANSACTIONS_ROUTES.TransactionsController)
      .when('/my/transactions/', TRANSACTIONS_ROUTES.TransactionsForUsersController)
      .otherwise({
        redirectTo: '/',
      });
  }).run(function ($rootScope, $location, FlashService) {
    $rootScope.logOut = function () {
      $rootScope.currentUser = null;
      FlashService.pushMessage('You have successfully logged out');
      $location.path('/');
    };
  });
