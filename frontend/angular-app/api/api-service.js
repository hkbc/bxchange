'use strict';

angular.module('services.API')
  .service('ApiService', function ($http, API_BASE_URL) {
    var exports = this;

    exports.request = function (options) {
      options.url = API_BASE_URL + options.url;
      return $http(options);
    };

    return exports;
  });
