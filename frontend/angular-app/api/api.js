'use strict';

angular.module('services.API', [])
  .constant('API_BASE_URL', 'https://hack.ngrok.com/daeus/battlehack/backend/battlehackapi');
