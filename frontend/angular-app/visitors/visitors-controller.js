'use strict';

angular.module('visitors')
  .controller('VisitorsController', function ($scope, $location, UsersService, 
        FlashService) {
      $scope.googleLogin = function () {
        UsersService.authenticateWith('gplus')
          .then(function (user) {
            FlashService.pushMessage('You have successfully logged in as ' + user.first_name + ' ' + user.last_name);
            $location.path('/');
          }, function (data) {
            console.error(data);
          });
      };
    });


