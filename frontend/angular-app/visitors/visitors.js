'use strict';

angular.module('visitors', ['ngRoute', 'users'])
  .constant('VISITORS_ROUTES', {
    VisitorsController: {
      templateUrl: 'visitors/templates/index.html',
      controller: 'VisitorsController',
    }
  });
