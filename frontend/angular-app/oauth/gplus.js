'use strict';

angular.module('services.OAuth')
  .service('GplusService', function ($q, $http, ApiService) {
    var exports = this,
        clientId = '116691054888-v5p21ku7qie8m0i250m67rfgu45m96n1.apps.googleusercontent.com',
        apiKey = 'AIzaSyBu2DGmikXOrgK9jzFeJPyKz9W4Czru3w0',
        oauthScopes = 'https://www.googleapis.com/auth/plus.me email';

    angular.element(document).ready(function () {
      gapi.client.setApiKey(apiKey);
    });

    exports.authenticate = function () {
      var deferred = $q.defer();

      gapi.auth.authorize({
        client_id: clientId,
        scope: oauthScopes,
        immediate: false
      }, function (response) {
        if (!response.error) {
          gapi.client.load('plus', 'v1', function () {
            var request = gapi.client.plus.people.get({
              'userId': 'me',
            });
            request.execute(function (resp) {
              var httpRequest = ApiService.request({
                method: 'POST',
                url: '/login',
                params: {
                  email: resp.emails[0].value,
                  first_name: resp.name.givenName,
                  last_name: resp.name.familyName
                }
              }).success(function (data, status, headers, config) {
                deferred.resolve(data);
              });
            });
          });
        } else {
          deferred.reject(response.error);
        }
      });

      return deferred.promise;
    };


    return exports;
  });
