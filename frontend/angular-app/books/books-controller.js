'use strict';

angular.module('books')
  .controller('BooksController', function ($scope, collection, BooksService) {
    var uniqueIds = [];
    $scope.books = collection.all().filter(function (book) {
      if (uniqueIds.indexOf(book.id) === -1) {
        uniqueIds.push(book.id);
        return true;
      }

      return false;
    });
    $scope.outdated = false;


    $scope.refreshBookList = function () {
      $scope.outdated = false;
      BooksService.refresh();
    };


    $scope.$on(BooksService.event('update'), function (event, collection) {
      $scope.books = collection.all();
    });


    $scope.$on(BooksService.event('outdated'), function () {
      console.log('OUTDATED');
      $scope.outdated = true;
    });
  });
