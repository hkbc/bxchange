'use strict';

angular.module('books', ['services.API'])
  .constant('BOOKS_ROUTES', {
    BooksController: {
      templateUrl: 'books/templates/index.html',
      controller: 'BooksController',
      resolve: {
        collection: function (BooksService) {
          return BooksService.collection();
        },
      },
    },
    BooksShowController: {
      templateUrl: 'books/templates/show.html',
      controller: 'BooksShowController',
      resolve: {
        resolves: function ($route, $q, BooksService, TransactionsService) {
          var deferred = $q.defer(),
              obj = {};

          BooksService.find({ id: $route.current.params.id })
            .then(function (book) {
              obj.book = book;
              return TransactionsService.where({ id: book.transactions_id })
            })
            .then(function (transactions) {
              obj.transactions = transactions;
              deferred.resolve(obj);
            });

          return deferred.promise;
        },
      },
    },
  });
