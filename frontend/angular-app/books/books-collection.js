'use strict';

angular.module('books')
  .factory('BooksCollection', function () {
    function BooksCollection(books) {
      this._books = books;
    }


    BooksCollection.prototype.all = function () {
      return this._books;
    };


    BooksCollection.prototype.find = function (options) {
      for (var i = 0; i < this._books.length; i++) {
        for (var key in options) {
          if (this._books[i][key] === options[key]) {
            return this._books[i];
          }
        }
      }

      return null;
    };


    return BooksCollection;
  });
