'use strict';

angular.module('books')
  .controller('BooksShowController', function ($scope, resolves) {
    $scope.book = resolves.book;
    $scope.transactions = resolves.transactions;
  });
