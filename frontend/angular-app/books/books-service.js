'use strict';

angular.module('books')
  .service('BooksService', function ($timeout, $q, $rootScope, BooksCollection, ApiService) {
    var exports = this,
        _lastId = null,
        _pingInterval = 10000,
        _collection = null;


    exports.event = function (key) {
      switch (key) {
        case 'update':
          return 'books:list:update';

        case 'outdated':
          return 'books:list:outdated';
        default:
          return null;
      }
    };


    exports.collection = function () {
      var deferred = $q.defer();

      if (_collection) {
        deferred.resolve(_collection);
      } else {
        var unbind = $rootScope.$on(exports.event('update'), function (event, newCollection) {
          pingForUpdates();
          unbind();
          deferred.resolve(newCollection);
        });
        exports.refresh();
      }

      return deferred.promise;
    };


    exports.find = function (options) {
      var deferred = $q.defer();

      exports.collection().then(function (collection) {
        var book = collection.find(options);
        if (book) {
          deferred.resolve(book);
        } else {
          deferred.reject('Could not find book with search options:', options);
        }
      });

      return deferred.promise;
    };


    exports.refresh = function () {
      ApiService.request({ method: 'GET', url: '/get_available_book' })
        .then(function (response) {
          _collection = new BooksCollection(response.data);
          _lastId = response.data[0].transaction_id;
          $rootScope.$broadcast(exports.event('update'), _collection);
        }, function (response) {
          console.error(response);
        });
    };


    function pingForUpdates() {
      ApiService.request({ method: 'GET', url: '/latest_book' })
        .then(function (response) {
          if (!_lastId) {
            _lastId = response.data.transaction_id;
          } else if (_lastId !== response.data.transaction_id) {
            _lastId = response.data.transaction_id;
            $rootScope.$broadcast(exports.event('outdated'));
          }
          $timeout(pingForUpdates, _pingInterval);
        }, function (response) {
          console.error(response);
          $timeout(pingForUpdates, _pingInterval);
        });
    }


    return exports;
});
