'use strict';

angular.module('flash')
  .service('FlashService', function ($rootScope) {
    var exports = this;


    exports.event = function (key) {
      switch (key) {
        case 'new':
          return 'flash:message:new';

        default:
          throw 'Unrecognised event ' + key + ' for FlashService';
      }
    };


    exports.pushMessage = function (message) {
      $rootScope.$broadcast(exports.event('new'), message);
    };


    return exports;
  });
