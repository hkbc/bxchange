'use strict';

angular.module('flash')
  .directive('flashContainer', function ($rootScope, $timeout, FlashService) {
    function link($scope, element, attrs) {
      $scope.messages = [];

      $scope.clear = function () {
        $scope.messages = [];
      };

      $rootScope.$on(FlashService.event('new'), function (event, message) {
        $scope.messages.unshift(message);
        $timeout(function () {
          $scope.messages.pop();
        }, 5000);
      });
    }

    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'flash/templates/flash-container.html',
      link: link,
    };
  });
