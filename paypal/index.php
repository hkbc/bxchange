<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title></title>
</head>
<body>
	<form id="checkout" method="post" action="/checkout">
	  <div id="dropin"></div>
	  <input type="submit" value="Buy Me A Coffee">
	</form>
	<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
	<script src="script.js"></script>
</body>
</html>
