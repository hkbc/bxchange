<?php require('paypal_functions.php'); ?>
<?php include('header.php'); ?>
<span class="span4">
  </span>
  <span class="span5">
<?php 
/* Store the parameters needed for SetExpressCheckout call and the LIPP REST app credentials
 First clear any prior session parameters */

if(isset($_POST["PAYMENTREQUEST_0_ITEMAMT"]))
$_SESSION["Payment_Amount"]=$_POST["PAYMENTREQUEST_0_AMT"];

if(isset($_POST["currencyCodeType"]))
$_SESSION["currencyCodeType"]=$_POST["currencyCodeType"];

if(isset($_POST["paymentType"]))
$_SESSION["paymentType"]=$_POST["paymentType"];

if(isset($_POST["L_PAYMENTREQUEST_0_NAME0"]))
$_SESSION["L_PAYMENTREQUEST_0_NAME0"]=$_POST["L_PAYMENTREQUEST_0_NAME0"];

if(isset($_POST["L_PAYMENTREQUEST_0_NUMBER0"]))
$_SESSION["L_PAYMENTREQUEST_0_NUMBER0"]=$_POST["L_PAYMENTREQUEST_0_NUMBER0"];

if(isset($_POST["L_PAYMENTREQUEST_0_DESC0"]))
$_SESSION["L_PAYMENTREQUEST_0_DESC0"]=$_POST["L_PAYMENTREQUEST_0_DESC0"];

if(isset($_POST["L_PAYMENTREQUEST_0_QTY0"]))
$_SESSION["L_PAYMENTREQUEST_0_QTY0"]=$_POST["L_PAYMENTREQUEST_0_QTY0"];

if(isset($_POST["PAYMENTREQUEST_0_ITEMAMT"]))
$_SESSION["PAYMENTREQUEST_0_ITEMAMT"]=$_POST["PAYMENTREQUEST_0_ITEMAMT"];

if(isset($_POST["PAYMENTREQUEST_0_TAXAMT"]))
$_SESSION["PAYMENTREQUEST_0_TAXAMT"]=$_POST["PAYMENTREQUEST_0_TAXAMT"];

if(isset($_POST["PAYMENTREQUEST_0_SHIPPINGAMT"]))
$_SESSION["PAYMENTREQUEST_0_SHIPPINGAMT"]=$_POST["PAYMENTREQUEST_0_SHIPPINGAMT"];

if(isset($_POST["PAYMENTREQUEST_0_HANDLINGAMT"]))
$_SESSION["PAYMENTREQUEST_0_HANDLINGAMT"]=$_POST["PAYMENTREQUEST_0_HANDLINGAMT"];

if(isset($_POST["PAYMENTREQUEST_0_SHIPDISCAMT"]))
$_SESSION["PAYMENTREQUEST_0_SHIPDISCAMT"]=$_POST["PAYMENTREQUEST_0_SHIPDISCAMT"];

if(isset($_POST["PAYMENTREQUEST_0_INSURANCEAMT"]))
$_SESSION["PAYMENTREQUEST_0_INSURANCEAMT"]=$_POST["PAYMENTREQUEST_0_INSURANCEAMT"];

if(isset($_POST["PAYMENTREQUEST_0_AMT"]))
$_SESSION["PAYMENTREQUEST_0_AMT"]=$_POST["PAYMENTREQUEST_0_AMT"];

if(isset($_POST["LOGOIMG"]))
$_SESSION["LOGOIMG"]=$_POST["LOGOIMG"];

if(isset($_POST["PAYMENTREQUEST_0_ITEMAMT"]))
$_SESSION["L_PAYMENTREQUEST_0_AMT0"]=$_POST["PAYMENTREQUEST_0_ITEMAMT"];


?>
<!--Login options are Login with the Mercant or Login with PayPal-->
<table>
   <tr>
      <td>
         <form method="POST">
            <legend>Log in with MERCHANT</legend>
            <p><label for="email">Email:</label>
               <input type="text" name="email" id="email" required>
            </p>
            <p><label for="password">Password:</label>
               <input type="password" name="password" id="password" required>
            </p>
            <p><button type="submit" class="btn btn-primary btn-medium">Log In</button></p>
         </form>
         <p class="tiny"><a href="#">Create an account</a></p>
      </td>
      <td>
         <div class="or">or</div>
      </td>
      <td>
         <span id="myContainer" data-paypal-button="true"></span>
         <!-- Scripts for getting the Login with PayPal button-->
         <script src="https://www.paypalobjects.com/js/external/api.js"></script>
         <script>
            paypal.use( ["login"], function(login) {
              login.render ({
                "appid":"<?php echo(CLIENT_ID_SANDBOX)?>",
                "authend":"sandbox", <!-- For live remove authend paramter -->
                "scopes": "openid profile email address phone https://uri.paypal.com/services/paypalattributes https://uri.paypal.com/services/expresscheckout",
                "containerid": "myContainer",
                "locale": "en-us",
                "returnurl": "<?php echo('http://'.$_SERVER['HTTP_HOST'].preg_replace('/lipp.php/','loggedin.php',$_SERVER['SCRIPT_NAME']))?>"
              });
            });
         </script>
      </td>
   </tr>
</table>
</span>
<span class="span3">
</span>
<?php include('footer.php') ?>