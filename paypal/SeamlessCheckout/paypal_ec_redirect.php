<?php
require_once ("paypal_functions.php");

$returnURL = EC_RETURN_URL;
$cancelURL = CANCEL_URL;

//Use access token from Login with Paypal
if(isset($_SESSION['user']['access_token']))
$identityAccessToken = $_SESSION['user']['access_token'];


if(isset($identityAccessToken))
$resArray = CallShortcutExpressCheckout ($returnURL, $cancelURL, $identityAccessToken);
else
$resArray = CallShortcutExpressCheckout ( $returnURL, $cancelURL, '');
$ack = strtoupper($resArray["ACK"]);
if($ack=="SUCCESS" || $ack=="SUCCESSWITHWARNING")
{
	RedirectToPayPal ( $resArray["TOKEN"] );
} 
else  
{
	//Display a user friendly Error on the page using any of the following error information returned by PayPal
	$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
	$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
	$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
	$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
	
	echo "SetExpressCheckout API call failed. ";
	echo "Detailed Error Message: " . $ErrorLongMsg;
	echo "Short Error Message: " . $ErrorShortMsg;
	echo "Error Code: " . $ErrorCode;
	echo "Error Severity Code: " . $ErrorSeverityCode;
}

?>