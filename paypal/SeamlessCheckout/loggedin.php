<?php require 'paypal_functions.php'; ?>
	
<?php

	if ( isset($_GET['code']) ) {
		try {
			//Get access token from the authorization code
			$access_token = acquire_access_token( $_GET['code'] );

			if ( !isset($access_token) ) {
				throw new Exception( 'Failed to get access token' );
			}

			$_SESSION['access_token'] = $access_token;

			//Get user profile details using the access token
			$profile = acquire_paypal_user_profile( $access_token );

			if ( !isset($profile) ) {
				throw new Exception( 'Failed to get user profile' );
			}

			$_SESSION['username'] = $profile->given_name;
			$_SESSION['user'] = array (
				"email"          => $profile->email,
				"given_name"     => $profile->given_name,
				"family_name"    => $profile->family_name,
				"language"       => $profile->language,
				"phone_number"   => $profile->phone_number,
				"street_address" => $profile->address->street_address,
				"locality"       => $profile->address->locality,
				"region"         => $profile->address->region,
				"postal_code"    => $profile->address->postal_code,
				"country"        => $profile->address->country,
				"access_token"	 => $access_token
			);			
		?>
		<?php

		} catch( Exception $e ) {
			throw_error_in_console( $e->getMessage() );
		}
	}
?>
<div id="one"></div>
<script>
	<!--Close the Login with PayPal window and go to the Target Url-->
	window.opener.location.href ="paypal_ec_redirect.php";
	window.close();
</script>



