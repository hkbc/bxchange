<?php
//Seller sandbox credentials. 
define("PP_USER_SANDBOX",  "mystore_api1.brandnew.com");
define("PP_PASSWORD_SANDBOX",  "49GYQ3225M8EJCGH");
define("PP_SIGNATURE_SANDBOX",  "AFcWxV21C7fd0v3bYYYRCpSSRl31A0dOPZfxniUpjYeN4Qb8bOEl4O5J");

//Seller Live credentials.
define("PP_USER","seller_username_here");
define("PP_PASSWORD", "seller_password_here");
define("PP_SIGNATURE","seller_signature_here");

//REST App Credentials
//Sandbox Credentials
define("CLIENT_ID_SANDBOX", "AZyQdxBf-94reD1mR2vFxAsp73ybhZ2ZvHBdWjcGENzQcBRNqk5U-x7Xugrm");
define("CLIENT_SECRET_SANDBOX",  "EMj46RBrcbr5MxiFEUYcu1jSHjdSHwARjT2UBTDaHZopGNdOWdjZVHZDjegs");

//REST App Credentials
//Live Credentials
define("CLIENT_ID_LIVE","live_client_id_here");
define("CLIENT_SECRET_LIVE","live_client_secret_here");

//The URL in your application where the ExpressCheckout flow returns after success (RETURN_URL) and after cancellation of the order (CANCEL_URL) 
define("EC_RETURN_URL",'http://'.$_SERVER['HTTP_HOST'].preg_replace('/paypal_ec_redirect.php/','return.php',$_SERVER['SCRIPT_NAME']));
define("CANCEL_URL",'http://'.$_SERVER['HTTP_HOST'].preg_replace('/paypal_ec_redirect.php/','cancel.php',$_SERVER['SCRIPT_NAME']));

//Express Checkout URLs for Sandbox
define("PP_CHECKOUT_URL_SANDBOX", "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=");
define("PP_NVP_ENDPOINT_SANDBOX","https://api-3t.sandbox.paypal.com/nvp");

//Express Checkout URLs for Live
define("PP_CHECKOUT_URL_LIVE","https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=");
define("PP_NVP_ENDPOINT_LIVE","https://api-3t.paypal.com/nvp");

//Version of the APIs
define("API_VERSION", "109.0");

//Whether Sandbox environment is being used
define("SANDBOX_FLAG", "true");

//Proxy Config
define("PROXY_HOST", "127.0.0.1");
define("PROXY_PORT", "808");

//ButtonSource Tracker Code
define("SBN_CODE","PP-DemoPortal-CodeSamples-php");

//PayPal Token URLs Sandbox
define("TOKEN_SERVICE_SANDBOX_URL","https://www.sandbox.paypal.com/webapps/auth/protocol/openidconnect/v1/tokenservice");
define("ACQUIRE_USER_PROFILE_SANDBOX_URL","https://www.sandbox.paypal.com/webapps/auth/protocol/openidconnect/v1/userinfo?schema=openid&access_token=");

//PayPal Token URLs Live
define("TOKEN_SERVICE_LIVE_URL","https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/tokenservice");
define("ACQUIRE_USER_PROFILE_LIVE_URL","https://www.paypal.com/webapps/auth/protocol/openidconnect/v1/userinfo?schema=openid&access_token=");
?>